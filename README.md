# README #
Sample application for using the [Nox Engine](https://bitbucket.org/suttungdigital/nox-engine).

To build and run the project, apply the [instructions from NOX Engine](https://bitbucket.org/suttungdigital/nox-engine/src/master/README.md) to this project.
