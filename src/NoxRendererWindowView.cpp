/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "NoxRendererWindowView.h"

#include <nox/app/IContext.h>
#include <nox/app/graphics/IRenderer.h>
#include <nox/app/graphics/opengl/BackgroundGradient.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/logic/IContext.h>
#include <nox/logic/graphics/DebugRenderingEnabled.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/physics/Simulation.h>

NoxRendererWindowView::NoxRendererWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle):
	nox::window::RenderSdlWindowView(applicationContext, windowTitle),
	renderer(nullptr),
	camera(std::make_shared<nox::app::graphics::Camera>(this->getWindowSize())),
	listener("NoxRendererWindowView"),
	mouseJointId(-1),
	cameraPanning(false),
	cameraZoomSpeed(0.1f)
{
	this->log = applicationContext->createLogger();
	this->log.setName("NoxRendererWindowView");

	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);

	this->camera->setScale({1.0f, 1.0f});
	this->rootSceneNode = std::make_shared<nox::app::graphics::TransformationNode>();
}

bool NoxRendererWindowView::initialize(nox::logic::IContext* context)
{
	if (this->RenderSdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());

	return true;
}

void NoxRendererWindowView::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{
	assert(renderer != nullptr);

	const auto graphicsResourceDescriptor = nox::app::resource::Descriptor{"graphics/graphics.json"};
	renderer->loadTextureAtlases(graphicsResourceDescriptor, this->getApplicationContext()->getResourceAccess());
	renderer->setWorldTextureAtlas("graphics/testTextureAtlas");

	auto background = std::make_unique<nox::app::graphics::BackgroundGradient>();
	background->setBottomColor({0.0f, 0.0f, 0.0f});
	background->setTopColor({1.0f, 1.0f, 1.0f});
	renderer->setBackgroundGradient(std::move(background));

	renderer->setAmbientLightLevel(1.0f);
	renderer->setCamera(this->camera);
	renderer->setRootSceneNode(this->rootSceneNode);
	renderer->organizeRenderSteps();

	this->renderer = renderer;
}

void NoxRendererWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	this->RenderSdlWindowView::onWindowSizeChanged(size);

	this->camera->setSize(size);
}

void NoxRendererWindowView::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	this->RenderSdlWindowView::onEvent(event);

	using SceneNodeEdit = nox::logic::graphics::SceneNodeEdited;

	if (event->isType(SceneNodeEdit::ID))
	{
		auto nodeEvent = static_cast<SceneNodeEdit*>(event.get());

		if (nodeEvent->getEditAction() == SceneNodeEdit::Action::CREATE)
		{
			this->rootSceneNode->addChild(nodeEvent->getSceneNode());
		}
		else if (nodeEvent->getEditAction() == SceneNodeEdit::Action::REMOVE)
		{
			this->rootSceneNode->removeChild(nodeEvent->getSceneNode());
		}
	}
}

void NoxRendererWindowView::onMousePress(const SDL_MouseButtonEvent& event)
{
	if (event.button == SDL_BUTTON_LEFT && this->mouseJointId < 0)
	{
		auto physics = this->getLogicContext()->getPhysics();

		const glm::vec2 clickPos = this->convertMouseToWorld(glm::ivec2(event.x, event.y));

		const auto actorClicked = physics->findActorIntersectingPoint(clickPos, nox::logic::physics::PhysicalBodyType::DYNAMIC);

		if (actorClicked != nullptr)
		{
			auto actorPhysics = actorClicked->findComponent<nox::logic::physics::ActorPhysics>();

			if (actorPhysics != nullptr)
			{
				nox::logic::physics::TargetJointDefinition jointDefinition(actorClicked->getId(), clickPos, 1000.0f * actorPhysics->getMass());
				this->mouseJointId = physics->createTargetJoint(jointDefinition);
			}
		}
	}
	else if (event.button == SDL_BUTTON_RIGHT)
	{
		this->cameraPanning = true;
	}
}

void NoxRendererWindowView::onMouseRelease(const SDL_MouseButtonEvent& event)
{
	if (event.button == SDL_BUTTON_LEFT && this->mouseJointId >= 0)
	{
		auto physics = this->getLogicContext()->getPhysics();
		physics->removeJoint(this->mouseJointId);
		this->mouseJointId = -1;
	}
	else if (event.button == SDL_BUTTON_RIGHT)
	{
		this->cameraPanning = false;
	}
}

void NoxRendererWindowView::onMouseMove(const SDL_MouseMotionEvent& event)
{
	if (this->mouseJointId >= 0)
	{
		auto physics = this->getLogicContext()->getPhysics();
		const glm::vec2 clickPos = this->convertMouseToWorld(glm::ivec2(event.x, event.y));

		physics->setTargetJointTargetPosition(this->mouseJointId, clickPos);
	}

	if (this->cameraPanning == true)
	{
		const glm::vec2 mouseMove = glm::vec2(event.xrel, event.yrel * -1);

		glm::vec2 currentCameraPosition = this->camera->getPosition();

		glm::vec4 mouseRelativeMotion(0.0f, 0.0f, 0.0f, 1.0f);
		mouseRelativeMotion.x = mouseMove.x / this->camera->getScale().x;
		mouseRelativeMotion.y = mouseMove.y / this->camera->getScale().y;

		const float cameraRotation = this->camera->getRotation();
		glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(1), cameraRotation, glm::vec3(0.0, 0.0, 1.0));
		glm::vec4 cameraVector = rotationMatrix * mouseRelativeMotion;

		currentCameraPosition.x -= cameraVector.x;
		currentCameraPosition.y -= cameraVector.y;

		this->camera->setPosition(currentCameraPosition);
	}
}

void NoxRendererWindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	if (event.keysym.sym == SDLK_q && this->renderer != nullptr)
	{
		this->renderer->toggleDebugRendering();

		const auto debugRenderEvent = std::make_shared<nox::logic::graphics::DebugRenderingEnabled>(renderer->isDebugRenderingEnabled());
		this->getLogicContext()->getEventBroadcaster()->queueEvent(debugRenderEvent);
	}
}

void NoxRendererWindowView::onMouseScroll(const SDL_MouseWheelEvent& event)
{
	if (event.y != 0)
	{
		const auto zoom = static_cast<float>(event.y);

		const auto previousScale = this->camera->getScale();
		const auto scaleChange = zoom * this->cameraZoomSpeed * glm::length(previousScale);

		this->camera->setScale(previousScale + scaleChange);
	}
}

glm::vec2 NoxRendererWindowView::convertMouseToWorld(const glm::ivec2& mousePos) const
{
	// Translate to center of screen.
	glm::vec4 convertedCoordinate(
			static_cast<float>(mousePos.x) - static_cast<float>(this->getWindowSize().x) / 2.0f,
			static_cast<float>((mousePos.y - static_cast<int>(this->getWindowSize().y)) * -1) - static_cast<float>(this->getWindowSize().y) / 2.0f,
			1.0f,
			1.0f
	);

	// Scale to world units.
	convertedCoordinate.x /= this->camera->getScale().x;
	convertedCoordinate.y /= this->camera->getScale().y;

	// Rotate coordinates with the inverted camera rotation.
	float currentRotation = this->camera->getRotation();
	glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.0f), currentRotation, glm::vec3(0.0f, 0.0f, 1.0f));
	convertedCoordinate = rotationMatrix * convertedCoordinate;

	// Translate coordinates to inverse camera position.
	glm::vec2 currentCameraPosition = this->camera->getPosition();
	convertedCoordinate.x += currentCameraPosition.x;
	convertedCoordinate.y += currentCameraPosition.y;

	return glm::vec2(convertedCoordinate);
}
