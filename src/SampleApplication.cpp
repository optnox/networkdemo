/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/app/audio/PlaybackProcess.h>
#include <nox/app/audio/openal/OpenALSystem.h>
#include <nox/app/storage/DataStorageBoost.h>
#include <nox/logic/event/Manager.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/world/Manager.h>
#include <nox/app/net/UserData.h>

#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/actor/ActorGravitation.h>
#include <nox/logic/graphics/actor/ActorSprite.h>
#include "SampleApplication.h"

namespace event
{
	const nox::logic::event::Event::IdType TPS_UPDATE = "game.tps_update";
}

SampleApplication::SampleApplication():
	SdlApplication("Sample Game", "Suttung Digital"),
	listener(this->getName()),
	logic(nullptr),
	world(nullptr),
	window(nullptr),
    netView(nullptr)
{
}

bool SampleApplication::initializeResourceCache()
{
	const auto cacheSize = 512u;
	auto cache = std::make_unique<nox::app::resource::LruCache>(cacheSize);

	cache->setLogger(this->createLogger());

	this->logger.verbose().format("LRU Cache initialized with a size of %uMB", cacheSize);

	const auto engineAssetsDir = std::string{"nox-engine/assets"};
	const auto gameAssetsDir = std::string{"assets"};

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(engineAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for engine assets: %s", engineAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", engineAssetsDir.c_str());
	}

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(gameAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for game assets: %s", gameAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", gameAssetsDir.c_str());
	}

	cache->addLoader(std::make_unique<nox::app::resource::OggLoader>());
	this->logger.verbose().raw("Initialized ogg resource loader.");

	cache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));
	this->logger.verbose().raw("Initialized JSON resource loader.");

	this->setResourceCache(std::move(cache));

	return true;
}

bool SampleApplication::initializeDataStorage()
{
	auto dataStorage = std::make_unique<nox::app::storage::DataStorageBoost>();
	const std::string storageDir = this->getStorageDirectoryPath(false);

	if (storageDir.empty() == true)
	{
		this->logger.error().raw("Could not find a suitable storage directory.");
		return false;
	}
	else
	{
		try
		{
			dataStorage->initialize(storageDir);

			this->logger.verbose().format("Data storage initialized to \"%s\" directory.", storageDir.c_str());
			this->setDataStorage(std::move(dataStorage));
		}
		catch (nox::app::storage::DataStorageBoost::IntializationException& exception)
		{
			this->logger.error().format("Could not initialize data storage directory \"%s\": %s", storageDir.c_str(), exception.what());
			return false;
		}
	}

	return true;
}

bool SampleApplication::initializeAudio()
{
    auto audioSystem = std::make_unique<nox::app::audio::OpenALSystem>();

	if (audioSystem->initialize() == false)
	{
		return false;
	}

	this->setAudioSystem(std::move(audioSystem));

	return true;
}

bool SampleApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	this->logic = logic.get();

	this->addProcess(std::move(logic));

	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(this->logic);
	physics->setLogger(this->createLogger());
	this->logic->setPhysics(std::move(physics));

	auto worldHandler = std::make_unique<nox::logic::world::Manager>(this->logic);
	this->world = worldHandler.get();

	this->logic->setWorldHandler(std::move(worldHandler));

	this->world->registerActorComponent<nox::logic::actor::Transform>();
	this->world->registerActorComponent<nox::logic::physics::ActorPhysics>();
	this->world->registerActorComponent<nox::logic::physics::ActorGravitation>();
	this->world->registerActorComponent<nox::logic::graphics::ActorSprite>();

	auto actorDir = std::string{"actor"};
	this->logger.verbose().format("Loading actor definitions from resource directory \"%s\"", actorDir.c_str());
	this->world->loadActorDefinitions(this->getResourceAccess(), actorDir);

	this->logger.verbose().raw("Initialized logic");

	return true;
}

bool SampleApplication::initializeWindow()
{
	this->logger.verbose().raw("Initializing SDL video subsystem.");

	if (this->initializeSdlSubsystem(SDL_INIT_VIDEO) == false)
	{
		return false;
	}

	auto window = std::make_unique<NoxRendererWindowView>(this, this->getName());
	this->window = window.get();

	this->logic->addView(std::move(window));

	this->logger.verbose().raw("Initialized NoxRendererWindowView.");

	return true;
}

bool SampleApplication::initializeNetwork()
{
    std::vector<std::string> args = getApplicationArguments();

    bool client = false;
    bool server = false;

    for (std::string str : args)
    {
        if (str == "-s" || str == "--server")
            server = true;
        if (str == "-c" || str == "--client")
            client = true;
    }

    if (client == server)
    {
        return false;
    }

    auto p = &this->pktTrans;
    auto d = &this->decMgr;

    if (server)
    {
        auto snv = std::make_unique<nox::app::net::ServerNetworkView>(p, d);
        snv->startServer(8891);
        snv->setEnableDiscoveryBroadcast(true);

        this->netView = snv.get();
        this->logic->addView(std::move(snv));

    }
    else
    {
        nox::app::net::UserData userData;
        userData.setUserName("TestoramaDingDong");

        auto cnv = std::make_unique<nox::app::net::ClientNetworkView>(p);

        this->netView = cnv.get();
        this->logic->addView(std::move(cnv));

        nox::app::net::ClientNetworkView *client;
        client = (nox::app::net::ClientNetworkView*)this->netView;
        client->connectToServer("localhost", 8891, userData);
    }

    return true;
}

bool SampleApplication::loadWorld()
{
	const auto worldFileDescriptor = nox::app::resource::Descriptor{"world/sample.json"};
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->logger.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->logger.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			this->world->loadWorld(jsonData->getRootValue());
		}
	}

	this->logger.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());

	return true;
}

void SampleApplication::playTestSound()
{
	auto* resourceCache = this->getResourceAccess();

	if (resourceCache == nullptr)
	{
		this->logger.error().raw("Failed playing sound: No resource cache.");
	}
	else
	{
		auto audioHandle = resourceCache->getHandle(nox::app::resource::Descriptor("testSound.ogg"));

		if (audioHandle == nullptr)
		{
			this->logger.error().raw("Failed playing sound: Could not access resource.");
		}
		else
		{
			auto* audioSystem = this->getAudioSystem();

			if (audioSystem == nullptr)
			{
				this->logger.error().raw("Failed playing sound: No audio system.");
			}
			else
			{
				this->processManager.startProcess(std::make_unique<nox::app::audio::PlaybackProcess>(audioHandle, audioSystem, false));
			}
		}
	}
}

bool SampleApplication::onInit()
{
	this->SdlApplication::onInit();

	this->logger = this->createLogger();
	this->logger.setName("SampleApplication");

	this->setTpsUpdateInterval(std::chrono::milliseconds(500));
	this->tpsUpdateTimer.setTimerLength(std::chrono::milliseconds(500));

	if (this->initializeResourceCache() == false)
	{
		return false;
	}

	if (this->initializeDataStorage() == false)
	{
		return false;
	}

	if (this->initializeAudio() == false)
	{
		return false;
	}

	if (this->initializeLogic() == false)
	{
		return false;
	}

	if (this->initializeWindow() == false)
	{
		return false;
	}

	if (this->loadWorld() == false)
	{
		return false;
	}

    if (this->initializeNetwork() == false)
    {
        return false;
    }

	this->listener.setup(this, this->logic->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(event::TPS_UPDATE);
//	this->listener.addEventTypeToListenFor(nox::logic::event::EventManager::BROADCAST_COMPLETE_EVENT);

    // I prefer my ears non-bleeding
	// this->playTestSound();

	return true;
}

void SampleApplication::onDestroy()
{
	this->SdlApplication::onDestroy();

	this->listener.stopListening();
	this->logic = nullptr;

	this->logger.verbose().raw("Destroyed");
}

void SampleApplication::onUpdate(const nox::Duration& deltaTime)
{
	this->SdlApplication::onUpdate(deltaTime);

	this->tpsUpdateTimer.spendTime(deltaTime);

	if (this->tpsUpdateTimer.timerReached() == true)
	{
		this->tpsUpdateTimer.reset();

		this->logic->getEventBroadcaster()->queueEvent(event::TPS_UPDATE);
	}

	this->processManager.updateProcesses(deltaTime);

	if (this->window != nullptr)
	{
		this->window->render();
	}
}

void SampleApplication::onEvent(const SDL_Event& event)
{
	this->SdlApplication::onEvent(event);

	this->window->onSdlEvent(event);
}

void SampleApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{

}
