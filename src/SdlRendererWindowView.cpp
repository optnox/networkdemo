/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SdlRendererWindowView.h"

#include <nox/app/IContext.h>

SdlRendererWindowView::SdlRendererWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle):
	SdlWindowView(applicationContext, windowTitle, true),
	renderer(nullptr)
{
	this->log = applicationContext->createLogger();
	this->log.setName("SdlRendererWindowView");
}

SdlRendererWindowView::~SdlRendererWindowView()
{
}

bool SdlRendererWindowView::onWindowCreated(SDL_Window* window)
{
	this->renderer = SDL_CreateRenderer(window, -1, 0);

	if (this->renderer == nullptr)
	{
		this->log.fatal().format("Could not create SDL renderer: %s", SDL_GetError());
		return false;
	}
	else
	{
		this->log.verbose().raw("SDL renderer created.");
		return true;
	}
}

void SdlRendererWindowView::onUpdate(const nox::Duration& deltaTime)
{
}

void SdlRendererWindowView::onSdlEvent(const SDL_Event& event)
{
}

void SdlRendererWindowView::onDestroy()
{
	if (this->renderer != nullptr)
	{
		SDL_DestroyRenderer(this->renderer);
		this->renderer = nullptr;
		this->log.verbose().raw("SDL renderer destroyed.");
	}
}

void SdlRendererWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	if (this->renderer != nullptr)
	{
		SDL_RenderClear(this->renderer);

		SDL_Rect windowRect;
		windowRect.x = 0;
		windowRect.y = 0;
		windowRect.w = static_cast<int>(size.x);
		windowRect.h = static_cast<int>(size.y);

		SDL_SetRenderDrawColor(this->renderer, 0, 0, 0, 255);
		SDL_RenderFillRect(this->renderer, &windowRect);

		const unsigned int rectSize = 100;
		SDL_Rect rect;
		rect.x = static_cast<int>(size.x / 2 - rectSize / 2);
		rect.y = static_cast<int>(size.y / 2 - rectSize / 2);
		rect.w = static_cast<int>(rectSize);
		rect.h = static_cast<int>(rectSize);

		SDL_SetRenderDrawColor(this->renderer, 120, 0, 0, 255);
		SDL_RenderFillRect(this->renderer, &rect);

		SDL_RenderPresent(this->renderer);
	}
}
