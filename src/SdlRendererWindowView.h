/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_SDLRENDERWINDOWVIEW_H_
#define NOX_SDLRENDERWINDOWVIEW_H_

#include <nox/window/SdlWindowView.h>
#include <nox/app/log/Logger.h>
#include <SDL2/SDL_render.h>

class SdlRendererWindowView: public nox::window::SdlWindowView
{
public:
	SdlRendererWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle);
	virtual ~SdlRendererWindowView();

private:
	bool onWindowCreated(SDL_Window* window) override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onSdlEvent(const SDL_Event& event) override;
	void onDestroy() override;
	void onWindowSizeChanged(const glm::uvec2& size) override;

	SDL_Renderer* renderer;
	nox::app::log::Logger log;
};

#endif
